# Salet
A general client-side framework for cybertext interactive fiction games.

**Salet** is based upon [Undum,](https://github.com/idmillington/undum) rewritten in CoffeeScript and altered to its own needs.
It also uses some code from [Raconteur,](https://github.com/sequitur/raconteur) same deal.

## License

The code, documentation, styles, design and images are all distributed under the MIT license.
This permits you to modify and use them, even for commercial use.
A copy of the MIT license is found in the LICENSE file.

*Raconteur is copyright (c) 2015 Bruno Dias, released under the similar license terms.*

*Undum is copyright (c) 2009-2015 Ian Millington, released under the similar license terms.*

## List of contributors
The list is alphabetical.

* Alexander Yakovlev - Salet's original author
* Andrew Plotkin
* Bruno Dias - Raconteur's original author
* David Eyk
* Dmitry Eliseev
* Konstantin Kitmanov
* Ian Millington - Undum's original author
* Ivan Narozhny
* Juhana Leinonen
* Michael Neal Tenuis
* Selene
