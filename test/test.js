const assert = require('assert');
const browserEnv = require('browser-env');
const domify = require('domify');
const marked = require('marked');
const $ = require('jquery');

browserEnv();
domify('<p>Hello <em>there</em></p>');

const salet = require('../lib/index.js');

salet.game_id = "2829f25a-c77a-4957-9662-e32b082f9f05";
salet.game_version = "1.0";
salet.autosave = false;
salet.autoload = false;
window.i18n.push("en", {
  hello: "Hello world!",
  func: function(argument) {
    return "Hello "+argument;
  },
  arg: "Hello {word}!"
})

room("start", {
  dsc: ""
});

room("start2", {
  dsc: "This is the second room.",
  canExit: false
});

room("third", {
  dsc: "This is the third room.",
  tags: ["tag", "third"]
});

describe('salet', function() {
  salet.beginGame();

  it ('the game should be started', function() {
    assert.notEqual(salet, void 0, "Salet is initialized");
    return assert.equal(salet.current, "start", "Salet is in the room called 'start'");
  });

  it("Markdown is working", function() {
    return assert.equal(markdown("*hi*"), "<p><em>hi</em></p>\n", "Markdown is good");
  });

  it("Translations are good", function() {
    assert.equal("hello".l(), "Hello world!", "Translations were imported fine");
    assert.equal("func".l("world!"), "Hello world!", "Translations can be functions");
    assert.equal("arg".l({word: "world"}), "Hello world!", "Argument substitutions are good");
    assert.equal("choice".l({number: 2}), "Choice 2", "Default translations are good");
    return;
  });

  it("RNG is working", function() {
    assert.ok(salet.rnd.randf() >= 0, "Random float is not negative");
    assert.ok(salet.rnd.randf() < 1, "Random float is less than 1");
    assert.equal(salet.rnd.diceString("d1"), 1, "A d1 dice string returns 1");
    assert.equal(salet.rnd.diceString("1d1"), 1, "A 1d1 dice string returns 1");
    assert.equal(salet.rnd.diceString("1d1+1"), 2, "A 1d1+1 dice string returns 2");
    assert.ok(salet.rnd.diceString("d%") > 0, "A d% dice string returns a positive number");
    assert.ok(salet.rnd.diceString("d%") <= 100, "A d% dice string returns a number less than 100");
    assert.ok(salet.rnd.diceString("dF") >= -1, "A Fudge dice string returns a number more or equal to -1");
    return assert.ok(salet.rnd.diceString("dF") <= 1, "A Fudge dice string returns a number less or equal to -1");
  });

  it("Inventory", function() {
    var lamp;
    lamp = unit("lamp", {
      display: "lamp description",
      inv: function() {
        return "that's a lamp";
      }
    });
    assert.equal(salet.character.has("lamp"), false, "The character has no lamp");
    salet.character.take(lamp);
    assert.equal(salet.character.has("lamp"), true, "The character has the lamp now");
    assert.equal(salet.character.inv("lamp"), "that's a lamp", "The lamp has an inventory action");
    assert.ok(listinv("lamp").match("lamp description"), "The lamp has a description");
    salet.character.drop("lamp");
    assert.equal(salet.character.has("lamp"), false, "The character has no lamp again");
    return;
  });

  it("Units in rooms", function() {
    var lamp;
    lamp = unit("lamp", {
      display: "lamp description",
      inv: function() {
        return "that's a lamp";
      }
    });
    salet.rooms["start2"].take(lamp);
    assert.equal(lamp.location, "start2", "The lamp unit is in 'start2' room");
    salet.rooms["start2"].drop("lamp");
    assert.equal(lamp.location, undefined, "The lamp unit is nowhere");
  });

  it("Localization", function() {
    assert.ok(window.i18n != null, "The localization is ready");
    window.i18n.push("ru", {
      "hello": "привет"
    });
    return assert.equal(window.i18n.localize("hello", "ru"), "привет", "The localization is working");
  });

  it("View", function() {
    salet.view.clearContent();
    assert.equal(jQuery("#content").html(), "", "View clears the content");
    salet.view.append("<p>hello</p>");
    return assert.equal(jQuery("#content").html(), "<p>hello</p>", "View writes new content");
  });

  setTimeout(function() {
    salet.goTo("start2");

    it("Salet enters second room", function() {
      assert.equal(salet.current, "start2", "This is the second room");
      return assert.equal($("#content").text(), "This is the second room.\n", "The room description is printed ok.");
    });

    it("Player visited the second room", function() {
      assert.equal(salet.isVisited("start2"), true, "Salet assures us the player visited the second room");
      assert.equal(salet.isVisited("third"), false, "Salet assures us the player did not visit the third room yet");
    });

    it("The second room forbids exiting (boolean)", function() {
      salet.goTo("start");
      return assert.equal(salet.current, "start2", "This is still the second room");
    });

    it("goBack() returns the player back", function() {
      salet.goBack()
      return assert.equal(salet.current, "start2", "The player went back as intended");
    });

    it("The second room forbids exiting (function)", function() {
      salet.rooms["start2"].canExit = function() {
        return false;
      };
      salet.goTo("start2");
      salet.goTo("start");
      return assert.equal(salet.current, "start2", "This is still the second room");
    });

    it("The second room is linked to first one and vice versa", function() {
      salet.rooms["start2"].canExit = true;

      salet.rooms["start2"].bilink("start");
      assert.equal(salet.rooms["start2"].ways[0], "start", "Second room is linked to the first one");
      assert.equal(salet.rooms["start"].ways[0], "start2", "First room is linked to the second one");
    });
  }, 100);

});
