###
Fast performance pseudo random number generator.

Not the cryptographic-level randomness, but enough for most games.

Code from CoffeeScript Cookbook:
https://coffeescript-cookbook.github.io/chapters/math/generating-predictable-random-numbers
###

class Random
  constructor: (@seed) ->
    @multiplier = 1664525
    @modulo = 4294967296 # 2**32-1;
    @offset = 1013904223
    unless @seed? && 0 <= @seed < @modulo
      @seed = (new Date().valueOf() * new Date().getMilliseconds()) % @modulo

  # sets new seed value
  seed: (seed) ->
    @seed = seed

  getSeed: () ->
    return @seed

  # return a random integer 0 <= n < @modulo
  randn: ->
    # new_seed = (a * seed + c) % m
    @seed = (@multiplier*@seed + @offset) % @modulo

 # return a random float 0 <= f < 1.0
  randf: ->
    this.randn() / @modulo

  # return a random int 0 <= f < n
  rand: (n) ->
    Math.floor(this.randf() * n)

  # return a random int min <= f < max
  randRange: (min, max) ->
    min + this.rand(max-min)

  randomInt: (upper) ->
    @rand(upper)

  # return a random element from an array
  randomElement: (elements) ->
    return elements[@rand(elements.length)]
  
  # return odds of a (value + dN) being larger than target, rounded down
  odds: (value, target, n) ->
    chance = target - value
    if chance <= 0
      return 100
    chance = Math.floor((1 - chance / n) * 100)
    if chance <= 0
      return 0
    return chance
  
  # Returns the result of rolling *n* dice with *dx* sides, and adding *plus*.
  dice: (n = 1, dx, plus = 0) ->
    result = 0
    for i in [0..n]
      result = 1 + Math.floor(@randf() * dx)
    if (plus)
      result += plus
    result

  # Rolls dice according to a xdy+z string, where
  # x and z are optional.
  # This rolls *x* dice of *y* sides and adds *z* to the result.
  # The *y* component can be "F" for Fudge die or "%" for percentile.

  diceString: (def) ->
    diceRe = /^([1-9][0-9]*)?d([%FA]|[1-9][0-9]*)([-+][1-9][0-9]*)?$/;
    match = def.match(diceRe)
    if (!match)
      throw new Error(
        "dice_string_error".l({string:def})
      )

    num = 1
    bonus = 0
    sides = undefined
    if match[1]
      num = parseInt(match[1], 10)
    if match[3]
      bonus = parseInt(match[3], 10)

    switch (match[2])
      when 'F'
        sides = 3
        bonus -= num*2
        break
      when '%'
        sides = 100
        break
      else
        sides = parseInt(match[2], 10)
        break
    return @dice(num, sides, bonus)
