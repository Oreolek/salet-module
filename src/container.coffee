# A container of elements.
class Container
  constructor: (spec) ->
    @inventory = []
    if spec?
      if spec.inventory?
        @inventory = spec.inventory
      if spec.units?
        @inventory = spec.units

    @take = (thing) =>
      if @name?
        thing.location = @name
      @inventory.push thing
      
    @drop = (thing) =>
      for i in @inventory
        if i.name == thing
          i.location = null
          @inventory.remove(i)
          return true
      return false

    @has = (thing) =>
      for i in @inventory
        if i.name == thing
          return true
      return false

    @inv = (thing) =>
      for i in @inventory
        if i.name == thing
          return i.inv.fcall(i)

    return this
