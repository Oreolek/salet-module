class Character extends Container
  constructor: (spec) ->
    super spec

    for index, value of spec
      this[index] = value
    return this

character = (spec) ->
  spec ?= {}
  return( new Character(spec) )
